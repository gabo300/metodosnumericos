
# coding: utf-8

# # Ejercicio 1
# Crea una matriz que simule un «tablero de ajedrez», tal que las casillas negras contienen 0 y las casillas blancas contienen un 1. Muestra la imagen y guárdala.

# In[2]:

import numpy as np
import cv2


# In[8]:


img = np.zeros((512,512,3), np.uint8) #crea una imagen negra de tamaño 512x512
#img = cv2.rectangle(img,(0,0),(64,64),(255,255,255),3)
img = cv2.rectangle(img,(0,128),(64,192),(255,255,255),3)
i=0
j=0
while j<8:
    while i<8:        
        img = cv2.rectangle(img,(j*64,64*i),((64*j)+64,(64*i)+64),(255,255,255),-1)
        img = cv2.rectangle(img,((j*64)+64,(64*i)+64),((j*64)+128,(64*i)+128),(255,255,255),-1)
        i+=2
    j+=2
    i=0
#para el rectángulo se pasan las esquinas superior izquierda e inferior
#derecha, el color y el espesor.
cv2.imshow('imagen',img)
cv2.waitKey(0) #si no se pone se congela y bloque la ventana
cv2.destroyAllWindows() #idem

