
# coding: utf-8

# # Acceder y modificar los valores de los píxeles
# 

# ## 1. Acceso y modificación de un píxel
# 
# Recordemos que una imagen digital es una matriz bidimensional. En el caso de una imagen a color, la imagen tendrá tres matrices bidimensionales que corresponden a los componentes $\verb|BGR|$

# In[ ]:


import cv2
import numpy as np

#primero cargamos la imagen
img = cv2.imread('supercow.jpg')
#accedemos a un píxel determinado a través de la indexación filas, columnas
#obteniendo una tupla BGR
px = img[100,100]
print(px)


# In[ ]:


#si queremos acceder solo al píxel azul
azul = img[100,100,0]
print(azul)


# In[ ]:


#también se pueden modificar los valores de los píxeles
img[100,100] = [255,255,255]
print(img[100,100])


# In[ ]:


#para trabajar sólo sobre una de las matrices de color se puede trabajar con item,
#que solo retorna un escalar (no un arreglo para BGR)
print(img.item(10,10,2))


# In[ ]:


#para modificarlo con la misma función
img.itemset((10,10,2),100)
print(img.item(10,10,2))


# In[ ]:


#Algunas propiedades de la imagen se pueden acceder al igual que un arreglo
print(img.shape)
print(img.size)
print(img.dtype)


# ## 2. Región de interés (ROI)
# 
# La región de interés es accesible a través de la indexación de un área completa.
# 
# **Ejemplo**
# 
# En la imagen a continuación tomaremos el balón naranja y lo replicaremos en otro lugar de la imagen.

# In[ ]:


import numpy as np
import cv2

img = cv2.imread('deportes.jpg')
cv2.imshow('imagen',img)
cv2.waitKey(0) & 0xFF 
cv2.destroyAllWindows()
#seleccionamos la ROI de la imagen, en este caso el balón naranja
balon = img[100:135, 350:390]
#replicamos el balón en otro lugar de la imagen
img[200:235, 100:140] = balon
#visualizamos la imagen modificada

cv2.imshow('imagen',img)
cv2.waitKey(0) & 0xFF 
cv2.destroyAllWindows()


# ## 3. Separación y unión de canales
# 
# Los canales B,G,R de una imagen se pueden dividir en componentes individuales cuando sea necesario. Igualmente es posible unirlos nuevamente para conformar la imagen BGR nuevamente.

# In[ ]:


#para la separación
#forma 1
b,g,r = cv2.split(img)
#forma 2
b = img[:,:,0]
#para la unión
#forma 1
img = cv2.merge((b,g,r))
#forma 2
img[:,:,2] = 255 # si deseamos poner todo el canal rojo en 255 (menor intensidad)
#las formas 2 suelen ser más eficientes en cuanto a carga computacional

cv2.imshow('imagen',img)
cv2.waitKey(0) & 0xFF 
cv2.destroyAllWindows()


# ** Ejercicio 1**
# 
# Busca las funciones de OpenCV que permitan el cambio de espacio de color de una imagen. Realiza el cambio de espacio de BGR a Escala de grises y a CMYK. Guarda ambas imágenes en archivos diferentes.

# In[ ]:


#espacio para solucionar el ejercicio 1


# **Ejercicio 2**
# 
# Toma una imagen que quieras editar y crea un efecto nuevo utilizando las propiedades de la región de interés. Guarda la imagen:

# In[ ]:


#espacio para la solución del ejercicio 2

