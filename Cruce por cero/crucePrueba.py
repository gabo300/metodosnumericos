import scipy.ndimage as nd #librería de señales
import numpy as np #librería numérica
import matplotlib.pyplot as plt #librería de graficación
from sympy import *

from functools import reduce
import operator

#Mostramos la imagen original y la imagen con el filtro Laplaciano
imagen  = plt.imread("Rombo 300Px.png")
umbral = 10**-5
imagen = imagen[:, :, 0]
plt.title("Imagen Original")
plt.rcParams['image.cmap'] = 'gray'
imagenFiltrada = nd.gaussian_laplace(imagen, 2) #aplicación del filtro Laplaciano de Gauss a la imagen de muestra


def crearLaImagenNegra(dimension):
    size = (dimension, dimension)
    return np.zeros(size)


def lagrange(xs, ys):
    obj = lambda: None

    def P(X):
        assert len(xs) == len(ys), 'len xs <> len ys'
        n = len(xs)

        def production(k):
            operands = [(X - xs[i]) / (xs[k] - xs[i]) for i in range(n) if k != i]
            return reduce(operator.mul, operands)

        t = sum(production(k) * ys[k] for k in range(n))
        return t

    obj.P = P
    return obj


def busquedaIncremental(p, initPoint, delta):
    cont = 0
    if (abs(p.P(initPoint)) <= umbral):  # Verificamos si el punto inicial dado es la raiz
        return initPoint
    newPoint = initPoint + delta  # Incrementamos el punto
    # Ciclo que se rompe hasta encontrar un cruce por cero
    #while ((int(p.P(newPoint)) * int(p.P(initPoint)) > 0)):
    while (True):
        if(int(p.P(newPoint)) * int(p.P(initPoint)) < 0):
            return newPoint
        cont += 1
        initPoint = newPoint
        newPoint += delta
        if (cont > 5):
            return None


imagenNegra = np.array(crearLaImagenNegra(imagenFiltrada.shape[0]))


#x = np. array([1,2,3,4,5])
for i in range(imagenFiltrada.shape[0]):
    for j in range(0,imagenFiltrada.shape[0],5):
        y = []
        x = []
        for k in range(5):
            x.append(j+k)
            y.append(imagenFiltrada[i][j+k])
        y = np.array(y)
        x = np.array(x)
        px = lagrange(x,y)
        crucePorCero = busquedaIncremental(px,j,1)
        #crucePorCero = bisección(px, [j,j+k])
        if(crucePorCero is not None):
                imagenNegra[i][int(crucePorCero)] = 1


plt.imshow(imagenNegra,vmin=0,vmax=1)
plt.show()