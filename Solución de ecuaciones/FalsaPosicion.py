from sympy import *
umbral = 10**-5
def falsaPosicion(funcion, intervalo):
    if(abs(funcion.subs("x",intervalo[0]))<=umbral):
        return intervalo[0]
    if(abs(funcion.subs("x",intervalo[1]))<=umbral):
        return intervalo[1]
    tano = (funcion.subs("x",intervalo[1])-funcion.subs("x",intervalo[0]))/ (intervalo[1]-intervalo[0])
    newPoint= intervalo[1] - (funcion.subs("x",intervalo[1])/tano)
    while True:
        if (abs(funcion.subs("x", newPoint)) <= umbral):
            break
        if (funcion.subs("x", intervalo[0]) * funcion.subs("x",newPoint ) > 0):
            intervalo[0] = newPoint
        else:
            intervalo[1] = newPoint
        tano = (funcion.subs("x",intervalo[1])-funcion.subs("x",intervalo[0]))/ (intervalo[1]-intervalo[0])
        newPoint= intervalo[1] - (funcion.subs("x",intervalo[1])/tano)
    return newPoint

funcion = Poly(input("Funcion "))
intervalo = [0,1]
intervalo[0] = int(input("Punto inicial "))
intervalo[1] = int(input("Punto final "))
print("Resultado ", falsaPosicion(funcion,intervalo))