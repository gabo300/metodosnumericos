from sympy import *
umbral = 10**-5
def bisección(funcion, intervalo):
    if (abs(funcion.subs("x", intervalo[0])) <= umbral):
        return intervalo[0]
    if (abs(funcion.subs("x", intervalo[1])) <= umbral):
        return intervalo[1]
    newPoint = (intervalo[0] + intervalo[1]) / 2
    while True:
        if (abs(funcion.subs("x", newPoint)) <= umbral):
           break
        else:
            if (funcion.subs("x", intervalo[0]) * funcion.subs("x",newPoint ) > 0):
                intervalo[0] = newPoint
            else:
                intervalo[1] = newPoint
            newPoint = (intervalo[0] + intervalo[1]) / 2

    return newPoint


funcion = Poly(input("Funcion "))
intervalo = [0,1]
intervalo[0] = int(input("Punto inicial "))
intervalo[1] = int(input("Punto final "))
print("Resultado ", bisección(funcion,intervalo))