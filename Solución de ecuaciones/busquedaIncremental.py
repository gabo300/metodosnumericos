from sympy import *
umbral = 10**-5

def busquedaIncremental(funcion, initPoint, delta):


    if(abs(funcion.subs("x",initPoint))<=umbral):
        return initPoint
    newPoint = initPoint + delta
    while((int(funcion.subs("x",newPoint))*int(funcion.subs("x",initPoint)))>0):
        initPoint = newPoint
        newPoint +=delta
    return newPoint

funcion = Poly(input("Funcion "))
initPoint = int(input("Punto inicial "))
delta = int(input("Incremento "))
print("Resultado ",busquedaIncremental(funcion, initPoint, delta))
