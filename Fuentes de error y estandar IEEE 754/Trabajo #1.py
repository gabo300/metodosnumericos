#Conversión IEEE 754
#Desarrolla un algoritmo que realice la conversión de un decimal ingresado por el usuario a formato IEEE 754 binario de
#32 bits y de un número IEEE 754 de 32 bits ingresado por el usuario a un decimal. **NO** es obligatorio utilizar las
#funciones que se encuentran a continuación.


def convertirABinario(decimal, precision):
    """Convierte la parte decimal de un numero en base 10 a binario.
    @Decimal: Es el la parte decimal del número entero
    @Precisión: Es el número de dígitos que tendrá el resultado"""
    aux = int(decimal)
    aux = aux * 2
    bindecimal = ""
    for i in range(precision):
        if (aux >= 10):
            bindecimal += "1"
            aux = aux - 10
        else:
            bindecimal += "0"
        aux = aux * 2
    return bindecimal
def convertirADecimal(binario):
    """Convierte la parte decimal de un número binario a un número en base 10
    @binario: El número binario a convertir"""
    decbinario = 0
    for i in range(len(binario)):
        if(binario[i]=="1"):
            decbinario += 2**((i+1)*-1)

    return decbinario


def decabin(entero, decimal,positivo):
    """Convierte un número en base 10 a un numero binario IEEE de 32 bits
    @Entero: Es la parte entera del número a convertir
    @Decimal: Es la parte decimal del número
    @Positivo: True si el número es positivo, falso para un número negativo"""
    binentero = int(bin(int(entero)).replace('0b', ''))
    mantisa=""
    bitsigno = ""
    exp = 0
    bindecimal = convertirABinario(decimal, 10)
    if "1" in str(binentero): #Si hay numero '1' a la izquierda
        for i in range(len(str(binentero)) - 1):
            if str(binentero)[i] == '1':
                exp = i
        for i in range(exp + 1, len(str(binentero))):
            mantisa += str(binentero)[i]
        mantisa += bindecimal
        exp = len(str(binentero)) - exp - 1
        exp =127+exp
    else:#Si hay numero '1' a la derecha
        for i in range(len(bindecimal)):
            if bindecimal[i] == '1':
                exp = i
                break
        for i in range(exp+1, len(bindecimal)):
            mantisa += bindecimal[i]
        exp=(exp+1)*-1
        exp = 127 + exp
    if(positivo):
        bitsigno="0"
    else:
        bitsigno ="1"
    exp = bin(int(exp)).replace('0b', '')
    if (len(exp) < 8):
        for i in range((len(exp)-8)*-1):
            exp = "0"+exp
    for i in range((len(mantisa)-23)*-1):
        mantisa+="0"
    return bitsigno+ "|" +exp+"|"+mantisa
print(decabin("0", "3",False))

def bindeca(bin):
    """Convierte un número binario IEEE de 321 bits en un número decimal"""
    bitsigno = bin[0]
    exp = (int(str(bin[1:9]), 2) -127)*-1
    mantisa = bin[9:32]

    mantisa = "1" + mantisa
    for i in range(exp-1):
        mantisa="0"+mantisa

    if(bitsigno=="1"):
        return  convertirADecimal(mantisa)*-1
    else:
        return convertirADecimal(mantisa)
    return
print("bindeca",bindeca("10111110100110011000000000000000"))














    

